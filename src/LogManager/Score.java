/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LogManager;
import java.io.Serializable;

public class Score  implements Serializable {
    /**
	 * Score class has one constructor and two getter methods.
     */
    private static final long serialVersionUID = 1L;
    private final int score;
    private final String naam;

    public int getScore() {
        return score;
    }

    public String getNaam() {
        return naam;
    }

    public Score(String naam, int score) {
        this.score = score;
        this.naam = naam;
    }
}
