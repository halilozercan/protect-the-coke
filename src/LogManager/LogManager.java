/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LogManager;

import GUI.GameScreen;
import GameManager.Mint;
import GameManager.Vect;
import GameManager.Weapon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LogManager {

    private static ArrayList<Score> scores;
    //Initialising an in and outputStream for working with the file
    public static ObjectOutputStream outputStream = null;
    public static ObjectInputStream inputStream;
    public static int wallLength = 100;
    public static int towerDistance = 70;
    public static int cokeRadius = 30;
    public static Integer level;
            
    @SuppressWarnings("empty-statement")
    public static void initLogManager(){
        if (!new File("ProtectTheCoke/scores.dat").isFile()){
            //Create high scores file.
            Path path = Paths.get("ProtectTheCoke/scores.dat");
            try {
                Files.createDirectories(path.getParent());
            } catch (IOException e1) {
            }
            try {
                Files.createFile(path);
            } catch (IOException e) {
                System.err.println("already exists: " + e.getMessage());
            }
        }
        if (!new File("ProtectTheCoke/level.dat").isFile()){
            //Create the file which keeps the available levels.
            Path path = Paths.get("ProtectTheCoke/level.dat");
            try {
                Files.createDirectories(path.getParent());
            } catch (IOException e1) {
            }
            try {
                Files.createFile(path);
            } catch (IOException e) {
                System.err.println("already exists: " + e.getMessage());
            }
            
            level =1;
            try {
                outputStream = new ObjectOutputStream(new FileOutputStream("ProtectTheCoke/level.dat"));
                outputStream.writeObject(level);
            } catch (IOException ex) {
                Logger.getLogger(LogManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally {
                try {
                    if (outputStream != null) {
                        outputStream.flush();
                        outputStream.close();
                    }
                } catch (IOException e) {
                    System.out.println("[Laad] IO Error: " + e.getMessage());
                }
            }
         
        }
        LogManager.inputStream = null;
        //initialising the scores-arraylist
        scores = new ArrayList<Score>();
    }
    public static int getMaxLevel(){
        try {
            inputStream = new ObjectInputStream(new FileInputStream("ProtectTheCoke/level.dat"));
            level = (Integer) inputStream.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("[Laad] FNF Error: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("[Laad] IO Error: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("[Laad] CNF Error: " + e.getMessage());
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                System.out.println("[Laad] IO Error: " + e.getMessage());
            }
        }
        return level;
    }
    public static void updateMaxLevel(int changedLevel){
        level = changedLevel;
        File file = new File("ProtectTheCoke/scores.dat");
        file.delete();
        //Create the file which keeps the available levels.
        Path path = Paths.get("ProtectTheCoke/level.dat");
        try {
            Files.createDirectories(path.getParent());
        } catch (IOException e1) {
        }
        try {
            Files.createFile(path);
        } catch (IOException e) {
            System.err.println("already exists: " + e.getMessage());
        }
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream("ProtectTheCoke/level.dat"));
            outputStream.writeObject(level);
        } catch (FileNotFoundException e) {
            System.out.println("[Update] FNF Error: " + e.getMessage() + ",the program will try and make a new file");
        } catch (IOException e) {
            System.out.println("[Update] IO Error: " + e.getMessage());
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                System.out.println("[Update] Error: " + e.getMessage());
            }
        }
    }
    public static ArrayList<Score> getHighScores() {
        loadScoreFile();
        sort();
        return scores;
    }
    public static void loadScoreFile() {
        try {
            inputStream = new ObjectInputStream(new FileInputStream("ProtectTheCoke/scores.dat"));
            scores = (ArrayList<Score>) inputStream.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("[Laad] FNF Error: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("[Laad] IO Error: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("[Laad] CNF Error: " + e.getMessage());
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                System.out.println("[Laad] IO Error: " + e.getMessage());
            }
        }
    }

    private static void sort() {
        ScoreComparator comparator = new ScoreComparator();
        Collections.sort(scores, comparator);
    }

    public static void addScore(String name, int score) {
        loadScoreFile();
        scores.add(new Score(name, score));
        updateScoreFile();
    }

    public static void updateScoreFile() {
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream("ProtectTheCoke/scores.dat"));
            outputStream.writeObject(scores);
        } catch (FileNotFoundException e) {
            System.out.println("[Update] FNF Error: " + e.getMessage() + ",the program will try and make a new file");
        } catch (IOException e) {
            System.out.println("[Update] IO Error: " + e.getMessage());
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                System.out.println("[Update] Error: " + e.getMessage());
            }
        }
    }

    public static String getHighscoreString() {
        String highscoreString = "<html>";
        final int max = 10;

        ArrayList<Score> highscr;
        highscr = getHighScores();

        int i = 0;
        int x = highscr.size();
        if (x > max) {
            x = max;
        }
        while (i < x) {
            highscoreString += (i + 1) + "  " + highscr.get(i).getNaam() + "     " + highscr.get(i).getScore() + "<br>";
            i++;
        }
        return highscoreString + "</html>";
    }

    public static int getWeaponRadius(int kind) {
        // TODO Auto-generated method stub
        switch (kind) {
            case Weapon.Water:
                return 150;
            case Weapon.Alcohol:
                return GameScreen.dimension / 2;
            case Weapon.Acid:
                return GameScreen.dimension / 2;
        }
        return GameScreen.dimension / 2;
    }

    public static int getWeaponAttackSpeed(int kind) {
        // TODO Auto-generated method stub
        switch (kind) {
            case Weapon.Water:
                return 500;
            case Weapon.Alcohol:
                return 1000;
            case Weapon.Acid:
                return 1000;
        }
        return 0;
    }

    public static int getWallHealth() {
        return 100;
    }

    public static int getWallCount() {
        // TODO Auto-generated method stub
        return 6;
    }

    public static Vect getWallPosition1(int i) {
        // TODO Auto-generated method stub
        switch (i) {
            case 0:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(30)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(30)));
            case 1:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(330)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(330)));
            case 2:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(270)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(270)));
            case 3:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(210)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(210)));
            case 4:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(150)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(150)));
            case 5:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(90)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(90)));
        }
        return new Vect(GameScreen.dimension / 2, GameScreen.dimension / 2);
    }

    public static Vect getWallPosition2(int i) {
        // TODO Auto-generated method stub
        switch (i) {
            case 0:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(330)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(330)));
            case 1:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(270)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(270)));
            case 2:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(210)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(210)));
            case 3:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(150)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(150)));
            case 4:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(90)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(90)));
            case 5:
                return new Vect(GameScreen.dimension / 2 + wallLength * Math.cos(Math.toRadians(30)), GameScreen.dimension / 2 - wallLength * Math.sin(Math.toRadians(30)));
        }
        return new Vect(GameScreen.dimension / 2, GameScreen.dimension / 2);
    }

    public static int getTowerCount() {
        // TODO Auto-generated method stub
        return 6;
    }

    public static Vect getTowerPosition(int i) {
        // TODO Auto-generated method stub
        switch (i) {
            case 0:
                return new Vect(GameScreen.dimension / 2 + towerDistance * Math.cos(Math.toRadians(330)), GameScreen.dimension / 2 - towerDistance * Math.sin(Math.toRadians(330)));
            case 1:
                return new Vect(GameScreen.dimension / 2 + towerDistance * Math.cos(Math.toRadians(270)), GameScreen.dimension / 2 - towerDistance * Math.sin(Math.toRadians(270)));
            case 2:
                return new Vect(GameScreen.dimension / 2 + towerDistance * Math.cos(Math.toRadians(210)), GameScreen.dimension / 2 - towerDistance * Math.sin(Math.toRadians(210)));
            case 3:
                return new Vect(GameScreen.dimension / 2 + towerDistance * Math.cos(Math.toRadians(150)), GameScreen.dimension / 2 - towerDistance * Math.sin(Math.toRadians(150)));
            case 4:
                return new Vect(GameScreen.dimension / 2 + towerDistance * Math.cos(Math.toRadians(90)), GameScreen.dimension / 2 - towerDistance * Math.sin(Math.toRadians(90)));
            case 5:
                return new Vect(GameScreen.dimension / 2 + towerDistance * Math.cos(Math.toRadians(30)), GameScreen.dimension / 2 - towerDistance * Math.sin(Math.toRadians(30)));
        }
        return new Vect(GameScreen.dimension / 2, GameScreen.dimension / 2);
    }

    public static int getMintHealth(int kind) {
        // TODO Auto-generated method stub
        return 24;
    }

    public static double getMintRadius() {
        // TODO Auto-generated method stub
        return 20;
    }

    public static double getCokeRadius() {
        // TODO Auto-generated method stub
        return cokeRadius;
    }

    public static int getMintBounty(int kind) {
        // TODO Auto-generated method stub
        switch (kind) {
            case Mint.White:
                return 50;
            case Mint.Red:
                return 100;
            case Mint.Blue:
                return 200;
        }
        return 0;
    }

    public static int getWeaponsCount() {
        // TODO Auto-generated method stub
        return 3;
    }

    public static int getWeaponPrice(int i) {
        // TODO Auto-generated method stub
        switch (i) {
            case Weapon.Water:
                return 100;
            case Weapon.Alcohol:
                return 400;
            case Weapon.Acid:
                return 2000;
        }
        return 0;
    }

    public static String getWeaponName(int kind) {
        // TODO Auto-generated method stub
        switch (kind) {
            case Weapon.Water:
                return "Water";
            case Weapon.Alcohol:
                return "Alcohol";
            case Weapon.Acid:
                return "Acid";
        }
        return "";
    }

    public static String getWeaponDescription(int kind) {
        // TODO Auto-generated method stub
        switch (kind) {
            case Weapon.Water:
                return "Water is useless weapon";
            case Weapon.Alcohol:
                return "<html>deneme<br>deneme2</html>";
            case Weapon.Acid:
                return "<html>deneme<br>deneme2</html>";
        }
        return "";
    }

    public static double getTowerRadius() {
        // TODO Auto-generated method stub
        return 30;
    }

    public static int getMintScore(int kind) {
        // TODO Auto-generated method stub
        switch (kind) {
            case Mint.White:
                return 20;
            case Mint.Red:
                return 40;
            case Mint.Blue:
                return 80;
        }
        return 0;
    }

    public static int getMaxMintCount(int level) {
        // TODO Auto-generated method stub
        switch (level) {
            case 1:
                return 25;
            case 2:
                return 50;
            case 3:
                return 75;
            case 4:
                return 100;
            case 5:
                return 150;
            case 6:
                return 200;
            case 7:
                return 250;
            case 8:
                return 300;
            case 9:
                return 350;
            case 10:
                return 400;
            default:
                return 0;
        }
    }

    public static boolean isHighScore(int score) {
        // TODO Auto-generated method stub
        return true;
    }

    public static int getStartingGold(int level) {
        // TODO Auto-generated method stub
        switch (level) {
            case 1:
                return 200;
            case 2:
                return 300;
            case 3:
                return 400;
            case 4:
                return 500;
            case 5:
                return 800;
            case 6:
                return 1000;
            case 7:
                return 2000;
            case 8:
                return 2500;
            case 9:
                return 3000;
            case 10:
                return 5000;
            default:
                return 0;
        }
    }

    public static String getCreditsText() {
        // TODO Auto-generated method stub
        return "<html> -Credits-<br>Halil Ozercan<br>Yusuf Oncul<br>Merve Cam<br>Baransel Tekin<br></html>";
    }
    public static int getAvailableLevels(){
        return 10;
    }
    public static String getHelpText(){
        return "<html><font color=\"blue\">Welcome to Protect the Coke</font>" + 
                "<br><font color=\"red\">Weapon Types</font>"+
                "<br>Water Weapon: 100 golds"+
                "<br>Alcohol Weapon: 400 golds "+
                "<br>Acid Weapon: 2000 golds "+
                "<br><font color=\"red\">Mint Types:</font>"+
                "<br>Blue: Can be dissolved by 6 alcohol shots or 3 acid shots, Not affected by water" + 
                "<br>Red: Can be dissolved by 4 water shots or 2 alcohol shots or 1 acid shot " + 
                "<br>White: Can be dissolved by 2 water shots, 1 alcohol or acid shot </html>";
    }
}
