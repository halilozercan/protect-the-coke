package GameManager;

import LogManager.LogManager;

public class Wall extends GameObject{
	private int health;
	private boolean alive;
	
	public Wall(){
		this.health = LogManager.getWallHealth();
		this.alive = true;
	}
	public int getHealth(){
		return health;
	}
	
	public void setHealth(int health){
		if(health <= 0)
			this.alive = false;
		else
			this.alive = true;
		this.health = health;
	}
	
	public void restoreHealth(){
		this.health = LogManager.getWallHealth();
		this.alive = true;
	}
	
	public boolean isAlive(){
		return alive;
	}
	
}
