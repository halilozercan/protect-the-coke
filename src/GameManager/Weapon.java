package GameManager;

import LogManager.LogManager;

public class Weapon extends GameObject{
	public static final int Water = 0;
	public static final int Alcohol = 1;
	public static final int Acid = 2;
	public int kind;
	public int radius;
	public int attackspeed;
	public long lastAttackTime;
	
	public Weapon(int kind){
		this.kind = kind;
		radius = LogManager.getWeaponRadius(kind);
		attackspeed = LogManager.getWeaponAttackSpeed(kind);
	}
}
