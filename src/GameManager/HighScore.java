package GameManager;

public class HighScore {
	public HighScore(String name, int score){
		this.name = name;
		this.score = score;
	}
	public String name;
	public int score;
}
