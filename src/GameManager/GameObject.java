package GameManager;

public class GameObject {
	public static final int WALL = 1;
	public static final int TOWER = 2;
	protected int shape;
	protected Vect position1;
	protected Vect position2;
	protected double r;
	
	public int getShape(){
		return shape;
	}
	
	public Vect getPosition1(){
		return position1;
	}
	
	public Vect getPosition2(){
		return position2;
	}
	
	public double getR(){
		return r;
	}
	
	public void setShape(int shape){
		this.shape = shape;
	}
	
	public void setPosition1(Vect position1){
		this.position1 = position1;
	}
	
	public void setPosition2(Vect position2) {
		// TODO Auto-generated method stub
		this.position2 = position2;
	}
}
