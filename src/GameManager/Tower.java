package GameManager;

import LogManager.LogManager;

public class Tower extends GameObject{
	private Weapon weapon;
	private boolean isWeaponized;
	public Tower(){
		isWeaponized = false;
		weapon = null;
		r = LogManager.getTowerRadius();
	}
	
	public boolean isWeaponized(){
		return isWeaponized;
	}
	
	public Weapon getWeapon(){
		return weapon;
	}

	public boolean destroyWeapon() {
		// TODO Auto-generated method stub
		if(isWeaponized){
			weapon = null;
			isWeaponized = false;
			return true;
		}
		return false;
	}

	public boolean buildWeapon(int kind) {
		// TODO Auto-generated method stub
		if(!isWeaponized){
			weapon = new Weapon(kind);
			isWeaponized = true;
			return true;
		}
		return false;
	}
}
