package GameManager;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.Timer;

import LogManager.LogManager;
import GUI.GameScreen;
import GUI.GameScreen.MyFramePanel;

public class GameEngine {
	public static final int STATE_UNINITIATED = 0;
	public static final int STATE_PLAYING = 1;
	public static final int STATE_PAUSED = 2;
	
	private static final int TIME_STEP = 50;
	
	private int level, gameState;
	private BufferedImage mintImages[], weaponImages[], wallImage, towerImage, coinImage, bottleImage;
	private Timer timer;
	private GameMap GM;
	private MyFramePanel GS;
	
	public GameEngine(int level, MyFramePanel gameScreen) {
		// TODO Auto-generated constructor stub
		this.level = level;
		GS = gameScreen;
		timer = new Timer(TIME_STEP, new MyTimerListener());
		GM = new GameMap(level);
		timer.start();
		gameState = STATE_PLAYING;
		
	}
	private class MyTimerListener implements ActionListener{
		
		public void actionPerformed(ActionEvent event){
			if(gameState == STATE_PLAYING){
				
				GM = createMints(GM);
				GM = CollisionManager.attackMints(GM, TIME_STEP);
				GM = CollisionManager.attackTowers(GM, TIME_STEP);
				
				if(GM.isLost()){
					timer.stop();
				}
				if(GM.isWin()){
					timer.stop();
                                        LogManager.updateMaxLevel(level+1);
				}
				
				GS.updateNow();
				
				GM.setTime(GM.getTime() + TIME_STEP) ;
				
			}
		}
	}
	
	public GameMap getGameMap(){
		return GM;
	}
	
	//Creates new mints according to specified rules
	private GameMap createMints(GameMap map)
	{
		ArrayList<Mint> mints = map.getMints();
		int approMintCount = 1 + (int) ((map.getTime()/10000)*Math.sqrt(level));
		//
		if(mints.size()<approMintCount && map.getTime()%200==0 && map.getDeathMintCount()<LogManager.getMaxMintCount(level)){
			Random rand = new Random();
			int dir = rand.nextInt(4);
			Vect initPosition = new Vect(0,0);
			if(dir==0){
				initPosition = new Vect(rand.nextInt(GameScreen.dimension), 0);
			}else if(dir==1){
				initPosition = new Vect(GameScreen.dimension, rand.nextInt(GameScreen.dimension));
			}else if(dir==2){
				initPosition = new Vect(rand.nextInt(GameScreen.dimension), GameScreen.dimension);
			}else if(dir==3){
				initPosition = new Vect(0, rand.nextInt(GameScreen.dimension));
			}
			int newMintMaxKind = 1;
			if((60000/level)<map.getTime())
				newMintMaxKind = 3;
			else if((60000/level)/2<map.getTime())
				newMintMaxKind = 2;
			
			Mint newMint = new Mint(rand.nextInt(newMintMaxKind));
			newMint.setPosition1(initPosition);
			Vect speedVector = new Vect((GameScreen.dimension/2-initPosition.getX())/(TIME_STEP*200) / (newMint.getKind()+1) , (GameScreen.dimension/2-initPosition.getY())/(TIME_STEP*200) / (newMint.getKind()+1));
			newMint.setSpeedVector(speedVector);
			
			mints.add(newMint);
			map.setMints(mints);
		}
		
		return map;
	}
	
	public boolean destroyWeapon(int index)
	{
		return GM.destroyWeapon(index);
	}
	
	public boolean buildWeapon(int index, int kind)
	{
		return GM.buildWeapon(index, kind);
	}
	
	public void restoreWall(int index)
	{
		GM.restoreWall(index);
	}
	
	public int getStatus()
	{
		return gameState;
	}
	
	public boolean destroyMint(int index, boolean killed)
	{
		return GM.destroyMint(index, killed);
	}
	
	public int getLevel(){
		return level;
	}
	
	public int getState(){
		return gameState;
	}	
}
