package GameManager;

import java.util.ArrayList;

import LogManager.LogManager;

public class GameMap {
	private ArrayList<Tower> towers;
	private ArrayList<Mint> mints;
	private ArrayList<Wall> walls;
	private int level;
	private long time;
	private int lives;
	private int coins;
	private int score;
	private int deathMintCount;
	
	public GameMap(int level){
		this.level = level;
		initializeGameObjects();
		lives = 5;
		coins = LogManager.getStartingGold(level);
		score = 0;
		deathMintCount = 0;
	}
	
	private void initializeGameObjects(){
		int n;
		n = LogManager.getWallCount();
		walls = new ArrayList<Wall>();
		for(int i=0;i<n;i++){
			Wall temp = new Wall();
			temp.setShape(GameObject.WALL);
			temp.setPosition1(LogManager.getWallPosition1(i));
			temp.setPosition2(LogManager.getWallPosition2(i));
			temp.setHealth(LogManager.getWallHealth());
			walls.add(temp);
		}
		
		n = LogManager.getTowerCount();
		towers = new ArrayList<Tower>();
		for(int i=0;i<n;i++){
			Tower temp = new Tower();
			temp.setShape(GameObject.TOWER);
			temp.setPosition1(LogManager.getTowerPosition(i));
			towers.add(temp);
		}
		
		mints = new ArrayList<Mint>();
	}
	
	public ArrayList<Mint> getMints(){
		return mints;
	}
	
	public ArrayList<Tower> getTowers(){
		return towers;
	}
	
	public ArrayList<Wall> getWalls() {
		return walls;
	}
	
	public void setMints(ArrayList<Mint> newMints){
		mints = new ArrayList<Mint>(newMints);
	}

	public long getTime() {
		// TODO Auto-generated method stub
		return time;
	}
	
	public void setTime(long time){
		this.time = time;
	}

	public boolean destroyWeapon(int index) {
		// TODO Auto-generated method stub
		if(towers.get(index).isWeaponized())
			return towers.get(index).destroyWeapon();
		return false;
	}

	public boolean buildWeapon(int index, int kind) {
		// TODO Auto-generated method stub
		if(!towers.get(index).isWeaponized()){
			coins -= LogManager.getWeaponPrice(kind);
			return towers.get(index).buildWeapon(kind);
		}
		return false;
	}

	public void restoreWall(int index) {
		// TODO Auto-generated method stub
		walls.get(index).restoreHealth();
		
	}

	public boolean destroyMint(int index, boolean killed) {
		// TODO Auto-generated method stub
		if(mints.get(index)!=null){
			if(killed){
				earnCoins(LogManager.getMintBounty(mints.get(index).kind));
				gainScore(LogManager.getMintScore(mints.get(index).kind));
				deathMintCount += 1;
			}
			mints.remove(index);
			return true;
		}
		return false;
	}
	
	public void damageWall(int wallIndex, int mintKind){
		switch(mintKind){
			case Mint.White: walls.get(wallIndex).setHealth(walls.get(wallIndex).getHealth()-10); break;
			case Mint.Blue: walls.get(wallIndex).setHealth(walls.get(wallIndex).getHealth()-20); break;
			case Mint.Red: walls.get(wallIndex).setHealth(walls.get(wallIndex).getHealth()-30); break;
		}
	}

	public void loseLive() {
		// TODO Auto-generated method stub
		lives--;
	}

	public int getLives() {
		// TODO Auto-generated method stub
		return lives;
	}

	public boolean isLost() {
		// TODO Auto-generated method stub
		if(lives<=0)
			return true;
		return false;
	}

	public void earnCoins(int mintBounty) {
		// TODO Auto-generated method stub
		coins += mintBounty;
	}
	
	public void gainScore(int score){
		this.score += score;
	}
	
	public int getCoins(){
		return coins;
	}
	
	public int getScore(){
		return score;
	}
	
	public int getDeathMintCount(){
		return deathMintCount;
	}

	public boolean isWin() {
		// TODO Auto-generated method stub
		if(deathMintCount >= LogManager.getMaxMintCount(level)){
			return true;
		}
		return false;
	}

	
}
