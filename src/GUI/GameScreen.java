package GUI;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import GameManager.GameEngine;
import GameManager.Mint;
import GameManager.Tower;
import GameManager.Vect;
import GameManager.Wall;
import GameManager.Weapon;

import LogManager.LogManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class GameScreen extends JFrame {

    public static int dimension = 600;
    public static int frameDimensionX = 900;
    public static int frameDimensionY = 650;
    private MyFramePanel myFramePanel;

    public GameScreen(int level) {
        myFramePanel = new MyFramePanel(level);
        setSize(new Dimension(frameDimensionX, frameDimensionY));
        setResizable(false);

        setContentPane(myFramePanel);
        pack();
        setVisible(true);
    }

    public class MyFramePanel extends JPanel implements Updatable {

        private GameEngine GE;
        private MyGamePanel gamePanel;
        private MyGameMenuPanel gameMenuPanel;
        public MyFramePanel(int level) {
            GE = new GameEngine(level, this);
            gamePanel = new MyGamePanel();
            gameMenuPanel = new MyGameMenuPanel();
            this.setPreferredSize(new Dimension(frameDimensionX, frameDimensionY));
            this.setLayout(new BorderLayout());
            this.add(gamePanel, BorderLayout.LINE_END);
            this.add(gameMenuPanel, BorderLayout.LINE_START);
        }
        public void updateNow() {
            gameMenuPanel.updateNow();
            gamePanel.updateNow();
        }

        private class MyGameMenuPanel extends JPanel implements Updatable {

            //Left Menu Objects
        	private JButton nextLevelButton, newGameButton;
            private JPanel container;
            private ImageIcon coinIcon;
            private JLabel coinsLabel, scoresLabel, deathMintsLabel, levelLabel, remainingLivesLabel;
            //HighScore Frame Objects
            private JPanel getNamePanel;
            private JButton submit;
            private JTextField nameField;
            private JFrame highScoreFrame;

            public MyGameMenuPanel() {
                this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
                this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

                coinIcon = new ImageIcon("images/coin.png");

                levelLabel = new JLabel();
                remainingLivesLabel = new JLabel();
                coinsLabel = new JLabel();
                scoresLabel = new JLabel();
                deathMintsLabel = new JLabel();

                nextLevelButton = new JButton("Go to next level");
                newGameButton = new JButton("New Game");
                container = new JPanel();

                nextLevelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        // TODO Auto-generated method stub
                        GE = new GameEngine(GE.getLevel() + 1, MyFramePanel.this);
                    }
                });

                newGameButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        // TODO Auto-generated method stub
                        GE = new GameEngine(1, MyFramePanel.this);
                    }
                });

                this.add(levelLabel);
                this.add(Box.createRigidArea(new Dimension(0, 10)));
                levelLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

                this.add(remainingLivesLabel);
                this.add(Box.createRigidArea(new Dimension(0, 10)));
                remainingLivesLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

                this.add(coinsLabel);
                this.add(Box.createRigidArea(new Dimension(0, 10)));
                coinsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

                this.add(scoresLabel);
                this.add(Box.createRigidArea(new Dimension(0, 10)));
                scoresLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

                this.add(deathMintsLabel);
                this.add(Box.createRigidArea(new Dimension(0, 10)));
                deathMintsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

                this.add(container);
                container.setAlignmentX(Component.LEFT_ALIGNMENT);
                container.add(nextLevelButton);
                container.add(newGameButton);
                nextLevelButton.setVisible(false);
                newGameButton.setVisible(false);
            }

            public void updateNow() {
                levelLabel.setText("Level: " + GE.getLevel());

                remainingLivesLabel.setText("Lives: " + GE.getGameMap().getLives());

                coinsLabel.setText("Coins: " + GE.getGameMap().getCoins());
                coinsLabel.setIcon(coinIcon);

                scoresLabel.setText("Scores: " + GE.getGameMap().getScore());

                deathMintsLabel.setText("Death Mints: " + GE.getGameMap().getDeathMintCount() + " / " + LogManager.getMaxMintCount(GE.getLevel()));

                if (GE.getGameMap().isWin()) {
                    if (GE.getLevel() < 10) {
                        nextLevelButton.setVisible(true);
                    } else {
                        newGameButton.setVisible(true);
                        if (LogManager.isHighScore(GE.getGameMap().getScore())) {
                            registerHighScore();
                        }
                    }
                } else if (GE.getGameMap().isLost()) {
                    newGameButton.setVisible(true);
                    if (LogManager.isHighScore(GE.getGameMap().getScore())) {
                        registerHighScore();
                    }
                } else {
                    nextLevelButton.setVisible(false);
                    newGameButton.setVisible(false);
                }

            }

            private void registerHighScore() {
            	highScoreFrame = new JFrame("Enter your name");
                
                getNamePanel = new JPanel();
                nameField = new JTextField(20);
                submit = new JButton("Submit");
                
                submit.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LogManager.addScore(nameField.getText(), GE.getGameMap().getScore());
                        nameField.setVisible(false);
                        submit.setVisible(false);
                        
                        JLabel highScores = new JLabel(LogManager.getHighscoreString());
                        getNamePanel.add(highScores);
                        highScoreFrame.pack();
                    }
                });
                
                getNamePanel.add(nameField);
                getNamePanel.add(submit);
                
                highScoreFrame.setResizable(false);
                highScoreFrame.setContentPane(getNamePanel);
                highScoreFrame.pack();
                highScoreFrame.setVisible(true);

            }
        }

        private class MyGamePanel extends JPanel implements Updatable {

            private JButton buildWeaponButton;
            private JButton restoreWallsButton;
            private BufferedImage wallImage, towerImage, coinImage, bottleImage, bgImage;
            private BufferedImage[] mintImages = new BufferedImage[3];
            private BufferedImage[] weaponImages = new BufferedImage[3];
            private BufferedImage[] wallImages = new BufferedImage[6];
            private TowerPanel openTowerPanel;
            private WallPanel openWallPanel;

            public MyGamePanel() {
                try {
                    mintImages[0] = ImageIO.read(new File("images/mint-white.png"));
                    mintImages[1] = ImageIO.read(new File("images/mint-red.png"));
                    mintImages[2] = ImageIO.read(new File("images/mint-blue.png"));
                    weaponImages[0] = ImageIO.read(new File("images/weapon0.png"));
                    weaponImages[1] = ImageIO.read(new File("images/weapon1.png"));
                    weaponImages[2] = ImageIO.read(new File("images/weapon2.png"));

                    towerImage = ImageIO.read(new File("images/tower.png"));
                    bgImage = ImageIO.read(new File("images/bg.png"));
                    //coinImage = ImageIO.read(new File("coin.jpg"));
                    bottleImage = ImageIO.read(new File("images/bottle.png"));
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                this.setPreferredSize(new Dimension(GameScreen.dimension, GameScreen.dimension));

                this.addMouseListener(new MouseListener() {

                    @Override
                    public void mouseClicked(MouseEvent arg0) {
                        // TODO Auto-generated method stub
                        for (int i = 0; i < GE.getGameMap().getTowers().size(); i++) {
                            if (Vect.distance(new Vect(arg0.getX(), arg0.getY()), GE.getGameMap().getTowers().get(i).getPosition1()) < GE.getGameMap().getTowers().get(i).getR()) {
                                openTowerPanel = new TowerPanel(i, MouseInfo.getPointerInfo().getLocation());
                            }
                        }
                        for (int i = 0; i < GE.getGameMap().getWalls().size(); i++) {
                            if (Vect.distanceToLineSegment(GE.getGameMap().getWalls().get(i).getPosition1(), GE.getGameMap().getWalls().get(i).getPosition2(), new Vect(arg0.getX(), arg0.getY())) < 5) {
                                openWallPanel = new WallPanel(i, MouseInfo.getPointerInfo().getLocation());
                            }
                        }
                    }

                    @Override
                    public void mouseEntered(MouseEvent arg0) {
						// TODO Auto-generated method stub

                    }

                    @Override
                    public void mouseExited(MouseEvent arg0) {
						// TODO Auto-generated method stub

                    }

                    @Override
                    public void mousePressed(MouseEvent arg0) {
						// TODO Auto-generated method stub
                    	GE.getGameMap().loseLive();
                    }

                    @Override
                    public void mouseReleased(MouseEvent arg0) {
						// TODO Auto-generated method stub

                    }

                });
            }
            public void updateNow() {
                this.repaint();
            }

            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(bgImage, 0, 0, null);
                drawMints(g, GE.getGameMap().getMints());
                drawTowers(g, GE.getGameMap().getTowers());
                drawWalls(g, GE.getGameMap().getWalls());
                g.drawImage(bottleImage, dimension / 2 - bottleImage.getWidth() / 2, dimension / 2 - bottleImage.getHeight() / 2, null);

                if (GE.getGameMap().isWin()) {
                    g.setFont(new Font("TimesRoman", Font.BOLD, 80));
                    g.drawString("YOU PASSED!", 200, 200);
                }
            }

            private void drawMints(Graphics g, ArrayList<Mint> mints) {
                for (int i = 0; i < mints.size(); i++) {
                    g.drawImage(mintImages[mints.get(i).getKind()],
                            (int) mints.get(i).getPosition1().getX() - mintImages[mints.get(i).getKind()].getWidth() / 2, (int) mints.get(i).getPosition1().getY() - mintImages[mints.get(i).getKind()].getHeight() / 2, null);
                    g.setColor(new Color(255, 0, 0));
                    g.fillRect((int) mints.get(i).getPosition1().getX(), (int) mints.get(i).getPosition1().getY() - (int) mints.get(i).getR(), (int) (30 * ((double) mints.get(i).health / (double) LogManager.getMintHealth(0))), 5);
                    g.setColor(new Color(0, 0, 0));
                }
            }

            private void drawTowers(Graphics g, ArrayList<Tower> towers) {
                for (int i = 0; i < towers.size(); i++) {
                    g.drawImage(towerImage, (int) towers.get(i).getPosition1().getX() - towerImage.getWidth() / 2, (int) towers.get(i).getPosition1().getY() - towerImage.getHeight() / 2, null);
                    if (towers.get(i).isWeaponized()) {
                        g.drawImage(weaponImages[towers.get(i).getWeapon().kind], (int) towers.get(i).getPosition1().getX() - towerImage.getWidth() / 2 + 8, (int) towers.get(i).getPosition1().getY() - towerImage.getWidth() / 2 + 8, null);
                    }
                }
            }

            private void drawWalls(Graphics g, ArrayList<Wall> walls) {
                for (int i = 0; i < walls.size(); i++) {
                    if (walls.get(i).isAlive()) {
                        Graphics2D g2 = (Graphics2D) g.create();
                        g2.setStroke(new BasicStroke(5));
                        g2.setColor(new Color((int) (255 * ((double) walls.get(i).getHealth() / (double) LogManager.getWallHealth())), 0, 0));
                        g2.draw(new Line2D.Float((int) walls.get(i).getPosition1().getX(), (int) walls.get(i).getPosition1().getY(), (int) walls.get(i).getPosition2().getX(), (int) walls.get(i).getPosition2().getY()));

                    } else {
                        Graphics2D g2 = (Graphics2D) g.create();
                        g2.setStroke(new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));
                        g2.setColor(new Color(0, 0, 0));
                        g2.draw(new Line2D.Float((int) walls.get(i).getPosition1().getX(), (int) walls.get(i).getPosition1().getY(), (int) walls.get(i).getPosition2().getX(), (int) walls.get(i).getPosition2().getY()));

                    }

                }
            }

            private class TowerPanel extends JDialog {

                private Tower tower;
                private int TOWERPANEL_X = 300;
                private int TOWERPANEL_Y = 400;

                public TowerPanel(final int index, Point loc) {
                    this.tower = GE.getGameMap().getTowers().get(index);
                    this.addWindowFocusListener(new WindowFocusListener() {
                        @Override
                        public void windowLostFocus(WindowEvent e) {
                            TowerPanel.this.setVisible(false);
                            TowerPanel.this.dispose();
                        }

                        @Override
                        public void windowGainedFocus(WindowEvent e) {
                        }
                    });

                    JPanel panel = new JPanel();
                    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
                    panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

                    if (tower.isWeaponized()) {
                        panel.add(new JLabel(LogManager.getWeaponName(tower.getWeapon().kind), new ImageIcon("images/weapon" + tower.getWeapon().kind + ".png"), JLabel.LEFT));
                        panel.add(Box.createRigidArea(new Dimension(0, 10)));
                        panel.add(new JLabel(LogManager.getWeaponDescription(tower.getWeapon().kind)));
                        JButton destroyButton = new JButton("Destroy Weapon");
                        destroyButton.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent arg0) {
                                // TODO Auto-generated method stub
                                GE.destroyWeapon(index);
                                TowerPanel.this.dispose();
                            }
                        });
                        panel.add(Box.createRigidArea(new Dimension(0, 10)));
                        panel.add(destroyButton);
                    } else {
                        panel.add(new JLabel("No weapon has been constructed yet"));
                        panel.add(Box.createRigidArea(new Dimension(0, 10)));
                        for (int i = 0; i < LogManager.getWeaponsCount(); i++) {
                            JButton newWeapon = new JButton();
                            newWeapon.setIcon(new ImageIcon(weaponImages[i]));
                            if (GE.getGameMap().getCoins() >= LogManager.getWeaponPrice(i)) {
                                newWeapon.setEnabled(true);
                            } else {
                                newWeapon.setEnabled(false);
                            }
                            final int copyI = i;
                            newWeapon.addActionListener(new ActionListener() {

                                @Override
                                public void actionPerformed(ActionEvent arg0) {
                                    // TODO Auto-generated method stub
                                    GE.buildWeapon(index, copyI);
                                    TowerPanel.this.dispose();
                                }
                            });
                            panel.add(newWeapon);
                            panel.add(Box.createRigidArea(new Dimension(0, 10)));
                        }
                    }
                    panel.setPreferredSize(new Dimension(TOWERPANEL_X, TOWERPANEL_Y));

                    this.setLocation(loc);
                    this.add(panel);
                    this.pack();
                    this.setVisible(true);

                }
            }

            private class WallPanel extends JDialog {

                private Wall wall;
                private int WALLPANEL_X = 300;
                private int WALLPANEL_Y = 100;

                public WallPanel(final int index, Point loc) {
                    this.wall = GE.getGameMap().getWalls().get(index);
                    this.addWindowFocusListener(new WindowFocusListener() {
                        @Override
                        public void windowLostFocus(WindowEvent e) {
                            WallPanel.this.setVisible(false);
                            WallPanel.this.dispose();
                        }

                        @Override
                        public void windowGainedFocus(WindowEvent e) {
                        }
                    });

                    JPanel panel = new JPanel();
                    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
                    panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

                    panel.add(new JLabel("Wall's health: " + wall.getHealth() + "/" + LogManager.getWallHealth()));
                    panel.add(Box.createRigidArea(new Dimension(0, 10)));
                    JButton restoreWallButton = new JButton("Restore Wall");
                    restoreWallButton.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent arg0) {
                            // TODO Auto-generated method stub
                            GE.getGameMap().getWalls().get(index).restoreHealth();
                            WallPanel.this.dispose();
                        }
                    });
                    panel.add(restoreWallButton);

                    panel.setPreferredSize(new Dimension(WALLPANEL_X, WALLPANEL_Y));

                    this.setLocation(loc);
                    this.add(panel);
                    this.pack();
                    this.setVisible(true);

                }
            }
        }

    }

}