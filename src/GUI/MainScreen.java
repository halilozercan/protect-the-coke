package GUI;

import LogManager.LogManager;
import LogManager.Score;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.Border;


public class MainScreen extends JFrame {
    private JPanel mainPanel, choicesPanel, highScoresPanel, creditsPanel, helpPanel, levelPanel;

    public MainScreen() {
        LogManager.initLogManager();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Protect the Coke");
        mainPanel = new JPanel(new CardLayout());
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        initComponents();
        setResizable(false);

        setContentPane(mainPanel);
        pack();
        setVisible(true);

    }
    ActionListener backAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            // TODO Auto-generated method stub
            CardLayout cl = (CardLayout) (mainPanel.getLayout());
            cl.show(mainPanel, "choices");
        }
    };

    public void initComponents() {

        choicesPanel = new JPanel();
        choicesPanel.setLayout(new BorderLayout());
        choicesPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JButton playGameButton = new JButton("Play Game");
        playGameButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                CardLayout cl = (CardLayout) (mainPanel.getLayout());
                cl.show(mainPanel, "level");
            }
        });

        JButton creditsButton = new JButton("Credits");
        creditsButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                CardLayout cl = (CardLayout) (mainPanel.getLayout());
                cl.show(mainPanel, "credits");
            }
        });

        JButton helpButton = new JButton("Help");
        helpButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                CardLayout cl = (CardLayout) (mainPanel.getLayout());
                cl.show(mainPanel, "help");
            }
        });

        JButton highScoresButton = new JButton("High Scores");
        highScoresButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                CardLayout cl = (CardLayout) (mainPanel.getLayout());
                cl.show(mainPanel, "highscores");
            }
        });

        choicesPanel.add(highScoresButton, BorderLayout.SOUTH);
        choicesPanel.add(creditsButton, BorderLayout.WEST);
        choicesPanel.add(helpButton, BorderLayout.EAST);
        choicesPanel.add(playGameButton, BorderLayout.CENTER);

        mainPanel.add(choicesPanel, "choices");

        creditsPanel = new JPanel();
        creditsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        creditsPanel.add(new JLabel(LogManager.getCreditsText()));
        JButton backButton = new JButton("back");
        backButton.addActionListener(backAction);
        creditsPanel.add(backButton);

        mainPanel.add(creditsPanel, "credits");

        helpPanel = new JPanel();
        helpPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        helpPanel.add(new JLabel(LogManager.getHelpText()));
        JButton backButton1 = new JButton("back");
        backButton1.addActionListener(backAction);
        helpPanel.add(backButton1);

        mainPanel.add(helpPanel, "help");

        highScoresPanel = new JPanel();
        highScoresPanel.setLayout(new BoxLayout(highScoresPanel, BoxLayout.Y_AXIS));
        highScoresPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JLabel score = new JLabel(LogManager.getHighscoreString());
        highScoresPanel.add(score);
         
        JButton backButton2 = new JButton("back");
        backButton2.addActionListener(backAction);
        highScoresPanel.add(backButton2);

        mainPanel.add(highScoresPanel, "highscores");
		//Level Select Panel

        levelPanel = new JPanel();
        levelPanel.setLayout(new FlowLayout(5));
        for (int i = 1; i <= LogManager.getMaxLevel(); i++) {
            JButton levelButton = new JButton("" + (i));
            final int copyI = i;
            levelButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent arg0) {
                    // TODO Auto-generated method stub
                    GameScreen gs = new GameScreen(copyI);
                }
            });
            levelPanel.add(levelButton);
        }
        JButton backButton3 = new JButton("Back");
        backButton3.addActionListener(backAction);
        levelPanel.add(backButton3);

        mainPanel.add(levelPanel, "level");

    }

    public static void main(String args[]) {

        new MainScreen();
    }
}
