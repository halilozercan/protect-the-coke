package GUI;

public interface Updatable {
	public void updateNow();
}
