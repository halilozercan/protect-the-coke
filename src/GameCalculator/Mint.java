package GameCalculator;

import LogManager.LogManager;

public class Mint extends GameObject{
	public static final int White = 0;
	public static final int Red = 1;
	public static final int Blue = 2;
	private Vect speedVector;
	public int kind;
	public int health;
	public Mint(int kind){
		this.kind = kind;
		this.health = LogManager.getMintHealth(kind);
		this.r = LogManager.getMintRadius();
	}
	public Vect getSpeedVector(){
		return speedVector;
	}
	public void setSpeedVector(Vect speed){
		this.speedVector = speed;
	}
	public int getKind() {
		// TODO Auto-generated method stub
		return kind;
	}
	public void setHealth(int newHealth){
		this.health = newHealth;
	}
}
