package GameCalculator;

import java.util.ArrayList;

import GUI.GameScreen;
import LogManager.LogManager;

public class CollisionManager {
	public static GameMap attackMints(GameMap gameMap, long elapsedTime){
		
		for(int i=0;i<gameMap.getMints().size();i++){
			Vect speedVector =  gameMap.getMints().get(i).getSpeedVector();
			Vect positionVector = gameMap.getMints().get(i).getPosition1();
			speedVector = Vect.multiply(speedVector, (int)elapsedTime);
			Vect newPosition = Vect.add(speedVector, positionVector);
			gameMap.getMints().get(i).setPosition1(newPosition);
			
		}
		gameMap = mintAttacksWall(gameMap);
		gameMap = mintAttacksCoke(gameMap);
		return gameMap;
	}
	
	private static GameMap mintAttacksWall(GameMap gameMap) {
		for(int i=0;i<gameMap.getWalls().size();i++){
			int j = 0;
			while(j<gameMap.getMints().size()){
				if( gameMap.getWalls().get(i).isAlive() && Vect.distanceToLineSegment( gameMap.getWalls().get(i).getPosition1(), gameMap.getWalls().get(i).getPosition2(), gameMap.getMints().get(j).getPosition1() ) < gameMap.getMints().get(j).getR() ){
					gameMap.damageWall(i, gameMap.getMints().get(j).kind);
					gameMap.destroyMint(j, false);
				}
				else
					j++;
			}
		}
		return gameMap;
	}
	
	private static GameMap mintAttacksCoke(GameMap gameMap) {
		for(int i=0;i<gameMap.getWalls().size();i++){
			int j = 0;
			while(j<gameMap.getMints().size()){
				if( Vect.distance( gameMap.getMints().get(j).getPosition1() , new Vect(GameScreen.dimension/2, GameScreen.dimension/2) ) < gameMap.getMints().get(j).getR() + LogManager.getCokeRadius() ){
					gameMap.loseLive();
					gameMap.destroyMint(j, false);
				}
				else
					j++;
			}
		}
		return gameMap;
	}

	public static GameMap attackTowers(GameMap gameMap, long elapsedTime){
		ArrayList<Tower> towers = gameMap.getTowers();
		ArrayList<Mint> mints = gameMap.getMints();
		
		for(int i=0;i<towers.size();i++){
			if(towers.get(i).isWeaponized() && (gameMap.getTime()-towers.get(i).getWeapon().lastAttackTime)>towers.get(i).getWeapon().attackspeed){
				int radius = towers.get(i).getWeapon().radius;
				int targetIndex = getMintInRange(towers.get(i).getPosition1(), radius, mints);
				if(targetIndex!=-1){
					gameMap = towerAttacksMint(gameMap, i, targetIndex);
				}
			}
		}
		gameMap = clearDeathMints(gameMap);
		return gameMap;
	}
	
	private static int getMintInRange(Vect position, int range, ArrayList<Mint> mints){
		double minDistance = 0;
		int minIndex = -1;
		if(mints.size()>0){
			minDistance = Vect.distance(position, mints.get(0).getPosition1());
			minIndex = 0;
		}
		
		for(int i=0;i<mints.size();i++){
			if(Vect.distance(position, mints.get(i).getPosition1())<minDistance){
				minDistance = Vect.distance(position, mints.get(i).getPosition1());
				minIndex = i;
			}
		}
		if(minDistance<range)
			return minIndex;
		else
			return -1;
	}
	
	private static GameMap towerAttacksMint(GameMap gameMap, int towerIndex, int mintIndex){
		gameMap.getTowers().get(towerIndex).getWeapon().lastAttackTime = gameMap.getTime();
		int damage = calculateDamage(gameMap.getTowers().get(towerIndex).getWeapon().kind, gameMap.getMints().get(mintIndex).kind);
		gameMap.getMints().get(mintIndex).health -= damage;
		return gameMap;
	}
	
	private static int calculateDamage(int weaponKind, int mintKind){
		if(weaponKind==Weapon.Water){
			if(mintKind==Mint.White){
				return 6;
			}
			else if(mintKind==Mint.Red){
				return 3;
			}
			else if(mintKind==Mint.Blue){
				return 0;
			}
		}
		else if(weaponKind==Weapon.Alcohol){
			if(mintKind==Mint.White){
				return 12;
			}
			else if(mintKind==Mint.Red){
				return 6;
			}
			else if(mintKind==Mint.Blue){
				return 2;
			}
		}
		else if(weaponKind==Weapon.Acid){
			if(mintKind==Mint.White){
				return 12;
			}
			else if(mintKind==Mint.Red){
				return 12;
			}
			else if(mintKind==Mint.Blue){
				return 4;
			}
		}
		return 0;
	}
	
	private static GameMap clearDeathMints(GameMap gameMap){
		for(int i=0;i<gameMap.getMints().size();i++)
			if(gameMap.getMints().get(i).health<=0 ){
				gameMap.destroyMint(i, true);
				
			}
		return gameMap;	
	}
}
