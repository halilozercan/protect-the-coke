package GameCalculator;

public class Vect {
	private double x, y;
	
	public Vect(){
		
	}
	
	public Vect(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public void setX(double x){
		this.x = x;
	}
	
	public void setY(double y){
		this.y = y;
	}
	
	public static Vect multiply(Vect v, double a){
		
		return new Vect(v.getX()*a, v.getY()*a);
	}
	
	public static Vect add(Vect v1, Vect v2){
		return new Vect(v1.getX()+v2.getX() , v1.getY()+v2.getY());
	}
	
	public static double distance(Vect v1, Vect v2){
		return Math.sqrt((v1.getX()-v2.getX())*(v1.getX()-v2.getX()) + (v1.getY()-v2.getY())*(v1.getY()-v2.getY()));
	}
	
	public static double lengthSquare(Vect v1, Vect v2){
		return Math.abs((v1.getX()-v2.getX())*(v1.getX()-v2.getX()) + (v1.getY()-v2.getY())*(v1.getY()-v2.getY()));
	}
	
	public static double dotProduct(Vect v1, Vect v2){
		return v1.getX()*v2.getX() + v1.getY()*v2.getY();
	}
	
	public static double distanceToLineSegment(Vect v1, Vect v2, Vect p){
			  // Return minimum distance between line segment vw and point p
		double l2 = lengthSquare(v2, v1);  // i.e. |w-v|^2 -  avoid a sqrt
		if (l2 == 0.0) return distance(p, v1);   // v == w case
		// Consider the line extending the segment, parameterized as v + t (w - v).
		// We find projection of point p onto the line. 
		// It falls where t = [(p-v) . (w-v)] / |w-v|^2
		double t = dotProduct(add(p,multiply(v1,-1)), add(v2,multiply(v1,-1))) / l2;
		if (t < 0.0) return distance(p, v1);       // Beyond the 'v' end of the segment
		else if (t > 1.0) return distance(p, v2);  // Beyond the 'w' end of the segment
		Vect projection = add( v1 , multiply( add(v2,multiply(v1,-1)) , t ) );  // Projection falls on the segment
		return distance(p, projection);
	}
	
	
}
